"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const CCSynchronizer_1 = require("./CCSynchronizer");
class Catalog {
    static Initialize() {
        this._repository.movies = require("../data/movies.json");
        CCSynchronizer_1.CCSynchronizer.Initialize();
    }
    static addMovies(movies) {
        for (let movie of movies) {
            if (this.isNew(movie.imdb)) {
                this._repository.movies.push({
                    order: this._repository.movies.length,
                    imdb: movie.imdb,
                    magnet: movie.magnet
                });
            }
        }
        fs.writeFileSync("./data/movies.json", JSON.stringify(this._repository.movies));
    }
    static getTopMovie() {
        return this._repository.movies[0];
    }
    static listMetas() {
        let metas = [];
        for (let movie of this._repository.movies) {
            metas.push({
                id: movie.imdb,
                type: "movie",
                "isFree": true,
            });
        }
        return metas;
    }
    static getStream(imdb) {
        for (let movie of this._repository.movies) {
            if (movie.imdb === imdb) {
                return [movie.magnet];
            }
        }
        return [];
    }
    static isNew(imdb) {
        for (let movie of this._repository.movies) {
            if (movie.imdb === imdb) {
                return false;
            }
        }
        return true;
    }
}
Catalog._repository = { movies: [] };
exports.Catalog = Catalog;
//# sourceMappingURL=Catalog.js.map